package com.example.pokedex.model;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "rel_pokemon_type")
@javax.persistence.SequenceGenerator(name = "sequence", sequenceName = "seq_rel_pokemon_type", allocationSize = 1)
@Getter @Setter @NoArgsConstructor
public class RelPokemonType extends DBEntity {
	
	
	@ManyToOne(cascade = { CascadeType.PERSIST }, targetEntity=Pokemon.class)
	@JoinColumn(name = "id_pokemon", nullable = false)
	private Pokemon pokemon;
	
	
	@ManyToOne(cascade = { CascadeType.PERSIST }, targetEntity=PokemonType.class)
	@JoinColumn(name = "id_pokemon_type", nullable = false)
	private PokemonType pokemonType;

}
