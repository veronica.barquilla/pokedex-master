package com.example.pokedex.model;



import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "member")
@javax.persistence.SequenceGenerator(name = "sequence", sequenceName = "seq_member", allocationSize = 1)
@Getter @Setter @NoArgsConstructor
public class Member extends DBEntity {

	private String name;

	
}
