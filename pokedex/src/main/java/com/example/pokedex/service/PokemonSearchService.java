package com.example.pokedex.service;

import java.util.List;

import com.example.pokedex.model.Pokemon;

public interface PokemonSearchService {

	List<Pokemon> searchByNameOrType(String name, Long type);

}
