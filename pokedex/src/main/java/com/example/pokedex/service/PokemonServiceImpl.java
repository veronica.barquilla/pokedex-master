package 
com.example.pokedex.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.pokedex.dao.PokemonDao;
import com.example.pokedex.dao.RelMemberPokemonFavDao;
import com.example.pokedex.model.Pokemon;
import com.example.pokedex.model.RelMemberPokemonFav;

@Service("pokemonService")
public class PokemonServiceImpl implements PokemonService {

	
	@Autowired
	private PokemonDao pokemonDao;
	
	@Autowired
	private RelMemberPokemonFavDao relMemberPokemonFavDao;
		
	@Autowired
	private ModelMapper modelMapper;
	
	
	@Override
	public List<Pokemon> getAll() {
		return pokemonDao.findAllByOrderByIdPokedexAsc();
	}
	
	@Override
	public List<Pokemon> getAllFavourite(Long idMember) {
		
		List<Pokemon> pokemonList = new ArrayList<>();
		
		try {
		List<RelMemberPokemonFav> pokemonFavList = relMemberPokemonFavDao.findByMemberIdOrderByPokemonIdPokedex(idMember);				
		
		
			for(RelMemberPokemonFav pokemonFav : pokemonFavList) {
				pokemonList.add(modelMapper.map(pokemonFav.getPokemon(), Pokemon.class));
			}	
		} catch(Exception e) {
			System.out.println();
		}
		
		
		return pokemonList;
	}

	@Override
	public Pokemon getById(long idPokedex, Long idMember) {
		return pokemonDao.getByIdPokedexOrderByIdPokedexAsc(idPokedex);	
	}
	
	@Override
	public List<Pokemon> getByName(String name) {
		return pokemonDao.findByNameLikeIgnoreCase(name);
	}
	
	
		

}
