package com.example.pokedex.service;

import java.util.List;

import com.example.pokedex.model.PokemonType;

public interface PokemonTypeService {

	List<PokemonType> getAll();
	
	PokemonType getById(Long id);

}
