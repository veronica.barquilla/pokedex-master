package com.example.pokedex.service;

import com.example.pokedex.model.Pokemon;

public interface PokemonFavouriteService {

	Pokemon saveFavourite(Long idPokedex, Long idMember);

	boolean deleteFavourite(Long idPokedex, Long idMember);

	Boolean isFavourite(long idPokedex, Long idMember);

}
