package com.example.pokedex.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.pokedex.dao.RelPokemonTypeDao;
import com.example.pokedex.model.Pokemon;
import com.example.pokedex.model.RelPokemonType;

@Service("pokemonSearchService")
public class PokemonSearchServiceImpl implements PokemonSearchService{

	@Autowired
	private RelPokemonTypeDao relPokemonTypeDao;
	
	@Autowired
	private PokemonService pokemonService;
	
	@Override	
	public List<Pokemon> searchByNameOrType(String name, Long type) {
		
		List<Pokemon> pokemonList = new ArrayList<Pokemon>();
		
		List<RelPokemonType> relPokemonList = null;
		
		String nameLike = name;
		
		if(!StringUtils.isEmpty(name)) {
			nameLike = "%"+name+"%";
		}
		
		if(type > 0 && !StringUtils.isEmpty(name)) {
			relPokemonList = relPokemonTypeDao.findByPokemonTypeIdAndPokemonNameLikeIgnoreCaseOrderByPokemonIdPokedex(type, nameLike);
			for(RelPokemonType rel : relPokemonList) {
				pokemonList.add(rel.getPokemon());
			}
		} else if(type > 0 && StringUtils.isEmpty(name)){
			relPokemonList = relPokemonTypeDao.findByPokemonTypeIdOrderByPokemonIdPokedex(type);
			for(RelPokemonType rel : relPokemonList) {
				pokemonList.add(rel.getPokemon());
			}
		} else {
			pokemonList = pokemonService.getByName(nameLike);			
		}
		
		return pokemonList;
	}
}
