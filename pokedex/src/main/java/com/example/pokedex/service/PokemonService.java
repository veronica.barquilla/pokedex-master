package com.example.pokedex.service;

import java.util.List;

import com.example.pokedex.model.Pokemon;

public interface PokemonService {
	
	List<Pokemon> getAll();

	Pokemon getById(long idPokedex, Long idMember);
	
	List<Pokemon> getAllFavourite(Long idMember);

	List<Pokemon> getByName(String name);
}
