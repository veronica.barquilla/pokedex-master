package com.example.pokedex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.pokedex.dao.PokemonTypeDao;
import com.example.pokedex.model.PokemonType;

@Service("pokemonTypeService")
public class PokemonTypeServiceImpl implements PokemonTypeService {

	@Autowired
	private PokemonTypeDao pokemonTypeDao;
	
	@Override
	public List<PokemonType> getAll() {
		return pokemonTypeDao.findAll();
	}
	
	@Override
	public PokemonType getById(Long id) {
		return pokemonTypeDao.getById(id);
	}
}
