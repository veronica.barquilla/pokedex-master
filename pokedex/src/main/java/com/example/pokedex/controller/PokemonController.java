package com.example.pokedex.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.pokedex.dto.BasicResponseDto;
import com.example.pokedex.dto.PokemonBasicInfoDto;
import com.example.pokedex.dto.PokemonDetailsDto;
import com.example.pokedex.model.Pokemon;
import com.example.pokedex.service.PokemonFavouriteService;
import com.example.pokedex.service.PokemonService;
import com.example.pokedex.util.Transformers;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/v1/pokemon")
public class PokemonController {
	

	static final Long idMember = 1L;
	
	@Autowired
	private PokemonService pokemonService;
	
	@Autowired
	private PokemonFavouriteService pokemonFavouriteService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private Transformers transformers;
	
	@GetMapping(value = {"/"})
	@ResponseBody
	@Operation(summary = "Retrieve all pokemons")
	public ResponseEntity<BasicResponseDto> get () {
		
		BasicResponseDto resultDto = new BasicResponseDto();
		
		List<PokemonBasicInfoDto> pokemonBasicInfoDto = new ArrayList<>();
		List<Pokemon> pokemonList = new ArrayList<>();
		List<Pokemon> pokemonFavList = new ArrayList<>();
		
		try {
			pokemonList = pokemonService.getAll();			
			pokemonFavList = pokemonService.getAllFavourite(idMember);	
			
			if(!pokemonList.isEmpty()) {
				pokemonBasicInfoDto = transformers.transformPokemonListToPokemonBasicInfoDtoWithFavourites(pokemonList, pokemonFavList);
			}else {
				resultDto.setStatus(404);
				resultDto.setMessage(HttpStatus.NOT_FOUND.toString());
			}
			
			resultDto.setData(pokemonBasicInfoDto);
			
		} catch (Exception e) {
			resultDto = new BasicResponseDto(500, e.getMessage(), e.getMessage(),null);
			return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		}
		
		return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
	}	
	
	@GetMapping(value = {"/{idPokedex}"})
	@ResponseBody
	@Operation(summary = "Retrieve pokemon by id pokedex")
	public ResponseEntity<BasicResponseDto> getById (@PathVariable Long idPokedex) {
		
		BasicResponseDto resultDto = new BasicResponseDto();
		
		PokemonDetailsDto pokemonDetailsDto = new PokemonDetailsDto();
		Pokemon pokemon = null;
		
		try {
			
			pokemon = pokemonService.getById(idPokedex, idMember);			
			
			if(pokemon != null){
				pokemonDetailsDto = modelMapper.map(pokemon, PokemonDetailsDto.class);
				pokemonDetailsDto.setFavourite(pokemonFavouriteService.isFavourite(pokemon.getIdPokedex(), idMember));
				pokemonDetailsDto.setEvolution(transformers.getEvolutions(pokemon));
				resultDto.setData(pokemonDetailsDto);

			}else {
				resultDto.setStatus(404);
				resultDto.setMessage(HttpStatus.NOT_FOUND.toString());
			}
			
		} catch (Exception e) {
			resultDto = new BasicResponseDto(500, e.getMessage(), e.getMessage(), null);
			return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		}
		
		return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
	}	
	
	
	
}
