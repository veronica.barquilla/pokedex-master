package com.example.pokedex.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.pokedex.dto.BasicResponseDto;
import com.example.pokedex.dto.PokemonDetailsDto;
import com.example.pokedex.dto.PokemonFavouriteDto;
import com.example.pokedex.dto.SaveFavouriteDto;
import com.example.pokedex.model.Pokemon;
import com.example.pokedex.service.PokemonFavouriteService;
import com.example.pokedex.service.PokemonService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/v1/pokemonFavourite")
public class PokemonFavouriteController {

	static final Long idMember = 1L;
	
	@Autowired
	private PokemonFavouriteService pokemonFavouriteService;
	
	@Autowired
	private PokemonService pokemonService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	
	@GetMapping(value = {"/"})
	@ResponseBody
	@Operation(summary = "Retrieve all favourite pokemons")
	public ResponseEntity<BasicResponseDto> get () {
		
		BasicResponseDto resultDto = new BasicResponseDto();
		
		List<PokemonFavouriteDto> pokemonBasicInfoDto = new ArrayList<>();
		List<Pokemon> pokemonFavList = new ArrayList<>();
		
		try {
						
			pokemonFavList = pokemonService.getAllFavourite(idMember);		
			if(!pokemonFavList.isEmpty()) {
				for(Pokemon pokemon: pokemonFavList) {
					pokemonBasicInfoDto.add(modelMapper.map(pokemon, PokemonFavouriteDto.class));
				}				
			} else {
				resultDto.setStatus(404);
				resultDto.setMessage(HttpStatus.NOT_FOUND.toString());
			}
			
			resultDto.setData(pokemonBasicInfoDto);
			
		} catch (Exception e) {
			resultDto = new BasicResponseDto(500, e.getMessage(), e.getMessage(), null);
			return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		}
		
		return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
	}		

	@PostMapping(value = "/")
	@ResponseBody
	@Operation(summary = "Save pokemon as favourite by id pokedex")
	public ResponseEntity<BasicResponseDto> saveFavourite (@RequestBody SaveFavouriteDto dto) {
		
		BasicResponseDto resultDto = new BasicResponseDto();
		
		PokemonDetailsDto pokemonDetailsDto = new PokemonDetailsDto();
		Pokemon pokemon = new Pokemon();
		
		try {
			pokemon = pokemonFavouriteService.saveFavourite(dto.getIdPokedex(), idMember);
						
			if(pokemon != null) {
				pokemonDetailsDto = modelMapper.map(pokemon, PokemonDetailsDto.class);
				pokemonDetailsDto.setFavourite(true);
				resultDto.setStatus(201);
				resultDto.setMessage(HttpStatus.CREATED.toString());
				resultDto.setData(pokemonDetailsDto);
			} else {
				resultDto.setStatus(404);
				resultDto.setMessage(HttpStatus.NOT_FOUND.toString());
			}
		} catch (Exception e) {
			resultDto = new BasicResponseDto(500, e.getMessage(), e.getMessage(), null);
			return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		}
		
		return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		
	}
	
	@DeleteMapping(value = "/{idPokedex}")
	@ResponseBody
	@Operation(summary = "Delete favourite pokemon by id pokedex")
	public ResponseEntity<BasicResponseDto> deleteFavourite (@PathVariable Long idPokedex) {
		
		BasicResponseDto resultDto = new BasicResponseDto();
		
		try {
			boolean deleted = pokemonFavouriteService.deleteFavourite(idPokedex, idMember);
			if(!deleted) {
				resultDto.setStatus(404);
				resultDto.setMessage(HttpStatus.NOT_FOUND.toString());
			}
			
		} catch(Exception e) {
			resultDto = new BasicResponseDto(500, e.getMessage(), e.getMessage(), null);
			return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		}					
		
		return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		
	}
}
