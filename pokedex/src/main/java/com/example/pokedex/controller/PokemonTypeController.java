package com.example.pokedex.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.pokedex.dto.BasicResponseDto;
import com.example.pokedex.dto.PokemonTypeDto;
import com.example.pokedex.model.PokemonType;
import com.example.pokedex.service.PokemonTypeService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/v1/pokemonType")
public class PokemonTypeController {
	
	@Autowired
	private PokemonTypeService pokemonTypeService;
	
	@Autowired
	private ModelMapper modelMapper;

	
	@GetMapping(value = {"/"})
	@ResponseBody
	@Operation(summary = "Retrieve all types of pokemons")
	public ResponseEntity<BasicResponseDto> get () {
		
		BasicResponseDto resultDto = new BasicResponseDto();
		
		List<PokemonType> pokemonTypeList = new ArrayList<>();
		List<PokemonTypeDto> pokemonTypeDto = new ArrayList<>();
		
		try {
			pokemonTypeList = pokemonTypeService.getAll();
			for(PokemonType type : pokemonTypeList) {
				pokemonTypeDto.add(modelMapper.map(type, PokemonTypeDto.class));
			}
			
		} catch (Exception e) {
			resultDto = new BasicResponseDto(500, e.getMessage(), e.getMessage(), null);
			return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		}
		
		return ResponseEntity.status(resultDto.getStatus()).body(resultDto);	
	}	
	
	@GetMapping(value = {"/{idType}"})
	@ResponseBody
	@Operation(summary = "Retrieve type pokemon by id")
	public ResponseEntity<BasicResponseDto> getById (@PathVariable Long idType) {
		
		BasicResponseDto resultDto = new BasicResponseDto();
		
		PokemonType pokemonType = new PokemonType();
		PokemonTypeDto pokemonTypeDto = new PokemonTypeDto();
		
		try {
			pokemonType = pokemonTypeService.getById(idType);
			
			if(pokemonType != null) {
				pokemonTypeDto = modelMapper.map(pokemonType, PokemonTypeDto.class);
			}else {
				resultDto.setStatus(404);
				resultDto.setMessage(HttpStatus.NOT_FOUND.toString());
			}	
			resultDto.setData(pokemonTypeDto);
			
		} catch (Exception e) {
			resultDto = new BasicResponseDto(500, e.getMessage(), e.getMessage(), null);
			return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		}
		
		return ResponseEntity.status(resultDto.getStatus()).body(resultDto);		
	}	
	
}
