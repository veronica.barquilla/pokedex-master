package com.example.pokedex.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.pokedex.dto.BasicResponseDto;
import com.example.pokedex.dto.PokemonBasicInfoDto;
import com.example.pokedex.model.Pokemon;
import com.example.pokedex.service.PokemonFavouriteService;
import com.example.pokedex.service.PokemonSearchService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api/v1/pokemonSearch")
public class PokemonSearchController {

	static final Long idMember = 1L;
	
	@Autowired
	private PokemonSearchService pokemonSearchService;
	
	@Autowired
	private PokemonFavouriteService pokemonFavouriteService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping
	@ResponseBody
	@Operation(summary = "Search pokemon by name or/and type")
	public ResponseEntity<BasicResponseDto> get (@RequestParam(name = "name",required = false, defaultValue = StringUtils.EMPTY) String name, @RequestParam(name = "type",required = false, defaultValue = "0") Long type) {
		
		BasicResponseDto resultDto = new BasicResponseDto();
		
		List<PokemonBasicInfoDto> pokemonBasicInfoDto = new ArrayList<>();
		List<Pokemon> pokemonList = null;
		
		try {
			pokemonList = pokemonSearchService.searchByNameOrType(name, type);
			
			if(!pokemonList.isEmpty()) {
				for(Pokemon pokemon : pokemonList){
					PokemonBasicInfoDto dto = new PokemonBasicInfoDto();
					dto = modelMapper.map(pokemon, PokemonBasicInfoDto.class);
					dto.setFavourite(pokemonFavouriteService.isFavourite(pokemon.getIdPokedex(), idMember));
					pokemonBasicInfoDto.add(dto);
				}
			}else {
				resultDto.setStatus(404);
				resultDto.setMessage(HttpStatus.NOT_FOUND.toString());
			}
			resultDto.setData(pokemonBasicInfoDto);			

		} catch (Exception e) {
			resultDto = new BasicResponseDto(500, e.getMessage(), e.getMessage(), null);
			return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		}
		
		return ResponseEntity.status(resultDto.getStatus()).body(resultDto);
		
	}
}
