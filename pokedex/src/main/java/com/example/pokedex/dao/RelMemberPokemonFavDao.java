package com.example.pokedex.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.pokedex.model.RelMemberPokemonFav;

public interface RelMemberPokemonFavDao extends JpaRepository<RelMemberPokemonFav,Long> {
	
	List<RelMemberPokemonFav> findByMemberIdOrderByPokemonIdPokedex (Long id);
	
	RelMemberPokemonFav findBymemberIdAndPokemonIdPokedex(Long idMember, Long IdPokedex);

}
