package com.example.pokedex.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.pokedex.model.RelPokemonType;

public interface RelPokemonTypeDao extends JpaRepository<RelPokemonType,Long>{
	
	List<RelPokemonType> findByPokemonTypeIdAndPokemonNameLikeIgnoreCaseOrderByPokemonIdPokedex (Long type, String name);
	
	List<RelPokemonType> findByPokemonTypeIdOrderByPokemonIdPokedex (Long type);

}
