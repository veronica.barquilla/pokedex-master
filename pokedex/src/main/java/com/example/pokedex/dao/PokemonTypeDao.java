package com.example.pokedex.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.pokedex.model.PokemonType;

public interface PokemonTypeDao extends JpaRepository<PokemonType,Long> {

	PokemonType getById(Long id);
}
