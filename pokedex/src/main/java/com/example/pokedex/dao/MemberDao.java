package com.example.pokedex.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.pokedex.model.Member;

public interface MemberDao  extends JpaRepository<Member,Long> {

	Member getById(Long id);
}
