package com.example.pokedex.dto;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

@Data
public class PokemonTypeDto {

	@ApiModelProperty(hidden=true)
	private String id;
	
	private String type;
	
	@Getter(AccessLevel.NONE)
	private String resource;
	
	public String getResource() {
		UriComponents uri = ServletUriComponentsBuilder.fromCurrentRequest().build();
		
		UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
		        .newInstance()
		        .scheme(uri.getScheme())
		        .host(uri.getHost())
		        .port(uri.getPort())
		        .path("api/v1/pokemonType")
		        .fragment(uri.getFragment());
				
		
		resource = uriComponentsBuilder.path("/{id}").buildAndExpand(this.id).toString();
		return resource;
	}

}
