package com.example.pokedex.dto;


import java.util.List;
import java.util.Set;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

@Data
public class PokemonDetailsDto {
	
	
	private Long idPokedex;
	
	private String name;
	
	
	private Long combatPoints;	
	
	
	private Long healthPoints;	
	
	
	private String weightFrom;
	
	
	private String weightTo;	
	
	
	private String heightFrom;	
	
	
	private String heightTo;	
	
	
	private String imageUrl;	
	
	
	private String soundUrl;
	
	private Set<PokemonTypeDto> types; 
	
	private Boolean favourite;
	
	@Getter(AccessLevel.NONE)
	private String resource;
	
	private List<PokemonEvolutionDto> evolution;
	
	
	public String getResource() {
		UriComponents uri = ServletUriComponentsBuilder.fromCurrentRequest().build();
		
		UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
		        .newInstance()
		        .scheme(uri.getScheme())
		        .host(uri.getHost())
		        .port(uri.getPort())
		        .path("api/v1/pokemon")
		        .fragment(uri.getFragment());
				
		
		resource = uriComponentsBuilder.path("/{idPokedex}").buildAndExpand(this.idPokedex).toString();
		return resource;
	}
	

	
}
