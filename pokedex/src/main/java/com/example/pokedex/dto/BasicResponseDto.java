package com.example.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BasicResponseDto {

	
	@ApiModelProperty(notes = "Status for the response", required = true, example="200", position=2)
	private int status;
	
	@ApiModelProperty(notes = "Message for the response", required = true, example="OK, Unfound entity, Generic error,...", position=1)
	private String message;

	@ApiModelProperty(notes = "Error for the response", required = true, example="OK, UNFOUND_ENTITY, GENERIC_ERROR,...", position=3)
	private String error;

	@ApiModelProperty(notes = "Generic Object for the response that contains the result data", required = true, position=4)
	private Object data;

	
	@JsonIgnore
	private Integer httpStatus = 200;

	public BasicResponseDto() {	
		this.status = 200;
		this.error = "";
		this.message = "OK";
	}

	public BasicResponseDto(int httpStatus, String error, String message,Object data) {
		this.status = httpStatus;
		this.error = error;
		this.data = data;
		this.httpStatus = httpStatus;
		this.message = error;
	}

}
