package com.example.pokedex.dto;

import java.util.Set;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

@Data
public class PokemonFavouriteDto {


	private Long idPokedex;
	
	
	private String name;

	private Set<PokemonTypeDto> types; 

	private boolean favourite = true; 
	
	private String imageUrl;
	
	@Getter(AccessLevel.NONE)
	private String resource;
	
	
	public String getResource() {
		UriComponents uri = ServletUriComponentsBuilder.fromCurrentRequest().build();
		
		UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
		        .newInstance()
		        .scheme(uri.getScheme())
		        .host(uri.getHost())
		        .port(uri.getPort())
		        .path("api/v1/pokemon")
		        .fragment(uri.getFragment());
				
		
		resource = uriComponentsBuilder.path("/{idPokedex}").buildAndExpand(this.idPokedex).toString();
		return resource;
	}
}
