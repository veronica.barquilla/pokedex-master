package com.example.pokedex.aspects;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


@Aspect
@Component
@Order(0)
public class LogExecutionTimeAspect {
 
	private final Logger logger = LoggerFactory.getLogger(getClass());


	@Around("execution(* com.example.pokedex.controller..*(..))")
	public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
	
	String className = joinPoint.getSignature().getDeclaringTypeName();
	String methodName = joinPoint.getSignature().getName();
	String apiName = className + "."+ methodName;
	String argsJoin = Arrays.toString(joinPoint.getArgs());
	   
	HttpServletRequest request =
	       ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	
	
	   logger.info("----->>>>> URI: {}\nHOST: {} HttpMethod: {}\nAPI: {}\nArguments: {}\n",
	    request.getRequestURI(),
	       request.getHeader("host"),
	       request.getMethod(),
	       apiName,
	       argsJoin);
	
	long start = System.currentTimeMillis();
	
	   Object proceed = joinPoint.proceed();
	
	   long executionTime = System.currentTimeMillis() - start;
	   
	   logger.info("{} executed in {} ms. ",joinPoint.getSignature() , executionTime);
	
	   return proceed;
	}
}
