package com.example.pokedex.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.pokedex.dto.PokemonBasicInfoDto;
import com.example.pokedex.dto.PokemonEvolutionDto;
import com.example.pokedex.model.Pokemon;

@Component
public class Transformers {
	
	@Autowired
	private ModelMapper modelMapper;

	public List<PokemonBasicInfoDto> transformPokemonListToPokemonBasicInfoDtoWithFavourites( List<Pokemon> pokemonList, List<Pokemon> pokemonFavList) {
		
		List<PokemonBasicInfoDto> pokemonListDto = new ArrayList<>();
		
		Collections.sort(pokemonList, new Comparator<Pokemon>() {
			   @Override
			   public int compare(Pokemon o1, Pokemon o2) {
			       return Long.valueOf(o1.getIdPokedex()).compareTo(o2.getIdPokedex());
			   }
			});
		
		if(pokemonList.isEmpty()) {
			for(Pokemon pokemon : pokemonFavList) {
				PokemonBasicInfoDto dto = modelMapper.map(pokemon, PokemonBasicInfoDto.class);
				dto.setFavourite(true);
				pokemonListDto.add(dto);
			}
		}else {
			
			for(Pokemon pokemon : pokemonList) {
				PokemonBasicInfoDto dto = modelMapper.map(pokemon, PokemonBasicInfoDto.class);
				if(pokemonFavList.contains(pokemon)) {
					dto.setFavourite(true);
				}
				pokemonListDto.add(dto);
			}
		}
		
		
		return pokemonListDto;
	}
	
	public List<PokemonEvolutionDto> getEvolutions(Pokemon pokemon) {
		List<PokemonEvolutionDto> evolutionList = new ArrayList<>();
		
		if(!pokemon.getReferencesTo().isEmpty()) {
			PokemonEvolutionDto evol = new PokemonEvolutionDto();
			
			Pokemon myPokemon = pokemon.getReferencesTo().iterator().next();
			
			if(!myPokemon.getReferencesTo().isEmpty()) {
			
				Pokemon firstEvolution = myPokemon.getReferencesTo().iterator().next();
						
				evol.setIdPokedex(firstEvolution.getIdPokedex());
				evol.setName(firstEvolution.getName());
				
				evolutionList.add(evol);
			
				if(!firstEvolution.getReferencesFrom().isEmpty()) {
					PokemonEvolutionDto evolTo = new PokemonEvolutionDto();
					
					Pokemon secondEvolution = firstEvolution.getReferencesFrom().iterator().next();
					
					evolTo.setIdPokedex(secondEvolution.getIdPokedex());
					evolTo.setName(secondEvolution.getName());
					
					evolutionList.add(evolTo);
				}
			}
			
		}
		
		return evolutionList;
	}
}
